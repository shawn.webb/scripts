#!/usr/local/bin/zsh
# power.zsh
# Copyright (C) 2015 Shawn Webb <shawn.webb@hardenedbsd.org>
# License: 2-Clause BSD License
#
# This little script exports non-required pools and detaches their
# geli providers before shutting down or rebooting.

# This needs to be in a specific order as these pools depend on each
# other
pools=( \
	blackbox \
	thumbie_enc \
	thumbie \
)

function sanity_checks() {
	if [ ${UID} -gt 0 ]; then
		echo "[-] Run this script as root, stupid!"
		exit 1
	fi
}

function pool_exists() {
	pool=${1}

	val=$(zpool list -H ${pool} 2> /dev/null)

	if [ ! -z "${val}" ]; then
		return 0
	else
		return 1
	fi
}

function pool_encrypted() {
	pool=${1}

	val=$(zpool status ${pool} | grep -F ".eli" | awk '{print $1;}' | head -n 1)

	if [ ! -z "${val}" ]; then
		return 0
	else
		return 1
	fi
}

function unmount_pool() {
	pool=${1}
	encfile=""

	if pool_exists ${pool}; then
		echo "[+] Unmounting ${pool}"
		if pool_encrypted ${pool}; then
			encfile=$(mktemp -q -t poweroff)
			if [ -z "${encfile}" ]; then
				echo "    [-] Could not create temp file."
				exit 1
			fi

			zpool status ${pool} | grep -F ".eli" | awk '{print $1;}' > ${encfile}
		fi

		zpool export ${pool}
		if [ ! ${?} -eq 0 ]; then
			echo "    [-] Could not export the pool."
			exit 1
		fi

		if [ ! -z "${encfile}" ]; then
			for line in $(cat ${encfile}); do
				echo "    [*] Detaching geli provider ${line}."

				geli detach ${line}
				if [ ! ${?} -eq 0 ]; then
					echo "    [-] Could not detach geli provider ${line}."
					exit 1
				fi
			done

			rm -f ${encfile}
		fi
	fi
}

sanity_checks

for pool in ${pools}; do
	unmount_pool ${pool}
done

doreboot=0
while getopts 'r' o; do
	case "${o}" in
		r)
			doreboot=1
			;;
	esac
done

if [ ${doreboot} -gt 0 ]; then
	shutdown -r now
else
	shutdown -p now
fi
