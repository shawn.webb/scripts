#!/usr/local/bin/zsh

HBSDCTRL="/usr/sbin/hbsdcontrol"

dir=""

while getopts "d:" o; do
	case "${o}" in
		d)
			dir="${OPTARG}"
			;;
	esac
done

nomprotect=( \
)

nopageexec=( \
)

shlibrandom=( \
)

for f in ${nomprotect}; do
	f=${dir}/${f}
	echo "[*] Disabling PaX MPROTECT for ${f}"
	${HBSDCTRL} pax disable mprotect ${f}
	if [ ${?} -gt 0 ]; then
		echo "    [-] Failed"
		exit 1
	fi
done

for f in ${nopageexec}; do
	f=${dir}/${f}
	echo "[*] Disabling PaX PAGEEXEC for ${f}"
	${HBSDCTRL} pax disable pageexec ${f}
	if [ ${?} -gt 0 ]; then
		echo "    [-] Failed"
		exit 1
	fi
done

for f in ${shlibrandom}; do
	f=${dir}/${f}
	echo "[*] Enabling SHLIBRANDOM for ${f}"
	${HBSDCTRL} pax enable shlibrandom ${f}
	if [ ${?} -gt 0 ]; then
		echo "    [-] Failed"
		exit 1
	fi
done

for f in $(find ${dir}/usr/local/openjdk11/bin -type f); do
	${HBSDCTRL} pax disable pageexec ${f}
	${HBSDCTRL} pax disable mprotect ${f}
done
