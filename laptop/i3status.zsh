#!/usr/local/bin/zsh

echo '{ "version": 1 }'
echo '['
echo '[]'
while true; do
cat <<EOF
,[
	{
		"name": "battery",
		"full_text": "Battery: $(sysctl -n hw.acpi.battery.life)%"
	},
	{
		"name": "volume",
		"full_text": "Volume: $(mixer -s vol | awk '{print $2;}')"
	},
	{
		"name": "date",
		"full_text": "$(date '+%A, %d %b %Y')"
	},
	{
		"name": "time",
		"full_text": "$(date '+%T')"
	}
]
EOF
sleep 5
done
