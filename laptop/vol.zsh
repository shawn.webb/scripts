#!/usr/local/bin/zsh

amt="${1}"

if [ -z "${amt}" ]; then
	echo "USAGE: ${0} [+-]amt"
	exit 1
fi

mixer vol ${amt}
res=${?}

exit ${res}
