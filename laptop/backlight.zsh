#!/usr/local/bin/zsh

amt="${1}"

if [ -z "${amt}" ]; then
	echo "USAGE: ${0} [+-]amt"
	exit 1
fi

val=$(backlight -q)

val=$((${val} + ${amt}))

backlight ${val}
exit ${?}
