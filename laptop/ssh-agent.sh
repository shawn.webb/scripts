#!/bin/sh

tmpfile="${HOME}/tmp/agent.txt"
agentttl="86400"

start_agent() {
	local res

	rm -f ${tmpfile}

	ssh-agent -t ${agentttl} > ${tmpfile}
	res=${?}
	if [ ${res} -gt 0 ]; then
		return 1
	fi

	return 0
}

if [ -z "${SSH_AGENT_PID}" ]; then
	SSH_AGENT_PID=$(pgrep ssh-agent)
	if [ -z "${SSH_AGENT_PID}" ]; then
		start_agent
	fi
fi

cat ${tmpfile}
