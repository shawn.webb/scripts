#!/usr/local/bin/zsh -ex

route add -net 172.16.5.0/24 192.168.99.244
route add -net 192.168.90.0/24 192.168.99.244
route add -net 192.168.75.0/24 192.168.99.244
route add -net 192.168.10.0/24 192.168.99.244
route add -net 192.168.72.0/24 192.168.99.244
