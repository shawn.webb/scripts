#!/bin/sh

###############
#### SETUP ####
###############

# Protected network (LAN)
cybernet_lanbridge="bridge2"
cybernet_lantaps="tap10"

# pfsync net
cybernet_syncbridge="bridge3"
cybernet_synctaps="tap11"

# WAN side
cybernet_wanbridge="bridge4"
cybernet_wantaps="tap12 tap13"

# Dev VMs
cybernet_devbridge="bridge5"
cybernet_devtaps="tap14 tap15"

cybernet_bridges="${cybernet_lanbridge} ${cybernet_syncbridge} ${cybernet_wanbridge}"
cybernet_taps="${cybernet_lantaps} ${cybernet_synctaps} ${cybernet_wantaps}"

cloned_interfaces="${cloned_interfaces} ${cybernet_bridges}"
cloned_interfaces="${cloned_interfaces} ${cybernet_taps}"

###########################
#### CONFIGURE BRIDGES ####
###########################

#+++++++++++++
#++++ LAN ++++
#+++++++++++++

taps=""
for tap in ${cybernet_lantaps}; do
	taps="${taps} addm ${tap}"
done

ifconfig_bridge2="inet 172.16.20.254 netmask 255.255.255.0 ${taps}"

#++++++++++++++++
#++++ PFSYNC ++++
#++++++++++++++++

taps=""
for tap in ${cybernet_synctaps}; do
	taps="${taps} addm ${tap}"
done

ifconfig_bridge3="${taps}"

#+++++++++++++
#++++ WAN ++++
#+++++++++++++

taps=""
for tap in ${cybernet_wantaps}; do
	taps="${taps} addm ${tap}"
done

ifconfig_bridge4="inet 172.16.40.254 netmask 255.255.255.0 ${taps}"

#+++++++++++++++++
#++++ Dev VMs ++++
#+++++++++++++++++

taps=""
for tap in ${cybernet_devtaps}; do
	taps="${taps} addm ${tap}"
done

ifconfig_bridge5="inet 172.16.50.254 netmask 255.255.255.0 ${taps}"
