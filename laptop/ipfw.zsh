#!/usr/local/bin/zsh

ipfw="/sbin/ipfw"

#################
# Configuration #
#################

nats=( \
	192.168.7.0/24 \
	192.168.8.0/24 \
	192.168.10.0/24 \
	192.168.20.0/24 \
)

#############
# Prep work #
#############

kldstat -m ipfw > /dev/null 2>&1
if [ ${?} -gt 0 ]; then
	kldload ipfw
	if [ ${?} -gt 0 ]; then
		echo "[-] Could not load ipfw module" >&2
		exit 1
	fi
	kldload ipfw_nat
	if [ ${?} -gt 0 ]; then
		echo "[-] Could not load ipfw_nat module" >&2
		exit 1
	fi
fi

${ipfw} -f flush
if [ ${?} -gt 0 ]; then
	echo "[-] Could not flush the ruleset" >&2
	exit 1
fi

${ipfw} table $NATs list > /dev/null 2>&1
if [ ${?} -gt 0 ]; then
	${ipfw} table NATs destroy
	if [ ${?} -gt 0 ]; then
		echo "[-] Could not destroy NATs table" >&2
		exit 1
	fi
fi

${ipfw} table NATs create type addr
if [ ${?} -gt 0 ]; then
	echo "[-] Could not create NATs table" >&2
	exit 1
fi

foreach nat in ${nats}; do
	${ipfw} table NATs add ${nat}
	if [ ${?} -gt 0 ]; then
		echo "[-] Could not add ${nat} to NATs table" >&2
		exit 1
	fi
done

${ipfw} add check-state

${ipfw} nat 1 config if wlan0 same_ports reset
${ipfw} nat 2 config if em0 same_ports reset

${ipfw} add nat 1 all from 'table(NATs)' to any out
${ipfw} add nat 1 all from any to me in
${ipfw} add nat 2 all from 'table(NATs)' to any out
${ipfw} add nat 2 all from any to me in

${ipfw} add allow ip from any to any
