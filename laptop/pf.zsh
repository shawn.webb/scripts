#!/usr/local/bin/zsh

export PATH="/sbin:/bin:/usr/sbin:/usr/bin:/usr/local/sbin:/usr/local/bin"

function wait_ping() {
	while ! ping -c 1 -t 1 8.8.8.8 > /dev/null 2>&1; do
		sleep 5
	done
}

function get_def_route_iface() {
	netstat -rn4 | grep default | awk '{print $4;}'
}

function get_ip_addr() {
	iface=${1}

	ifconfig ${iface} inet | grep inet | awk '{print $2;}'
}

wait_ping
iface=$(get_def_route_iface)
ip=$(get_ip_addr ${iface})
echo "myip=\"${ip}\"" > /etc/pf.ip
echo "myiface=\"${iface}\"" >> /etc/pf.ip
pfctl -f /etc/pf.conf
