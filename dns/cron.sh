#!/bin/sh -ex

blocklists="abuse ads fraud malware phishing ransomware scam tracking"
blocked_tlds="zip"

allowlist="t.co"

if [ "$(id -u)" -ne "0" ]; then
	echo "[-] Must be run as root" 1>&2
	exit 1
fi

myzones="/usr/local/etc/void-zones/my_void_hosts.txt"
tldfile="/usr/local/etc/unbound/zones/tld_blocklist.txt"

cd /data/repos/dns/Blocklist.Site
git pull

truncate -s 0 ${myzones}
truncate -s 0 ${tldfile}

for l in ${blocklists}; do
	if [ -f "${l}.txt" ]; then
		grep '^0\.0\.0\.0' ${l}.txt >> ${myzones}
	fi
done

for l in ${allowlist}; do
	echo "1.1.1.1 ${l}" >> ${myzones}
done

for l in ${blocked_tlds}; do
	printf 'local-zone: "%s" static\n' "${l}" >> ${tldfile}
done

/usr/local/bin/void-zones-update.sh
/usr/sbin/service unbound restart
